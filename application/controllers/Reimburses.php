<?php

use GuzzleHttp\RedirectMiddleware;

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Reimburses extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('reimburseModel');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('file');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $view_data["reimburse"] = $this->reimburseModel->reimburseInfo();
        $this->template->rander('reimburse/index', $view_data);
    }

    public function formReimburse()
    {
        $view_data['jabatan'] = $this->reimburseModel->getJabatan();
        $view_data['members_dropdown'] = $this->Users_model->get_all_where(array("deleted" => 0, "user_type" => "staff"))->result();
        $view_data["category"] = $this->reimburseModel->categoryReimburse();
        $this->template->rander('reimburse/formReimburse', $view_data);
    }

    public function saveReimburse()
    {
        $this->form_validation->set_rules('No_reimburse', 'No_reimburse', 'required');
        $this->form_validation->set_rules('reimburse_roles_id', 'reimburse_roles_id', 'required');
        $this->form_validation->set_rules('nama_id', 'nama_id', 'required');
        $this->form_validation->set_rules('category_reimburse_id[]', 'category_reimburse_id[]', 'required');
        $this->form_validation->set_rules('amount[]', 'amount[]', 'required|numeric');
        $this->form_validation->set_rules('date_reimburse[]', 'date_reimburse[]', 'required');
        if ($this->form_validation->run == False) {
            $this->session->set_flashdata('error_message', 'Please List Every Field');
            redirect('Reimburses/formReimburse');
            exit;
        } else {
            // data
            $nm = $this->input->post('category_reimburse_id');

            $ct = count($_POST['category_reimburse_id']);

            $data = array();


            for ($a = 0; $a < $ct; $a++) {
                $dateReimburse = $_POST['date_reimburse'][$a];
                $date = date('Y-m-d');
                $date = strtotime($date);
                $date = strtotime('-7 day', $date);
                if ($dateReimburse < date('Y-m-d', $date)) {
                    $this->session->set_flashdata("error_message", "Reimburse Max 1 Week Ago");
                    redirect('Reimburses/formReimburse', 'refresh');
                } else {
                    $data[] = [
                        "nama_id"  => $this->input->post('nama_id'),
                        "No_reimburse"  => $this->input->post('No_reimburse'),
                        // "reimburse_roles_id" => $this->input->post('reimburse_roles_id'),
                        "category_reimburse_id"  => $_POST['category_reimburse_id'][$a],
                        "amount"  => $_POST['amount'][$a],
                        "date_reimburse"  => $_POST['date_reimburse'][$a],
                        "status_reimburse" => 'Pending',
                        "keterangan" => $_POST['keterangan'][$a],
                        "photo" => $_FILES['photo']['name'][$a]
                    ];
                }


                $count = count($_FILES['photo']['name']);

                for ($i = 0; $i < $count; $i++) :
                    $_FILES['userfile']['name']     = $_FILES['photo']['name'][$i];
                    $_FILES['userfile']['type']     = $_FILES['photo']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $_FILES['photo']['tmp_name'][$i];
                    $_FILES['userfile']['error']    = $_FILES['photo']['error'][$i];
                    $_FILES['userfile']['size']     = $_FILES['photo']['size'][$i];

                    $image_path = realpath(APPPATH . '../assets/reimburse/');

                    $config = [
                        'upload_path' => $image_path,
                        'allowed_types' => 'jpg|png|gif',
                        'overwrite' => TRUE
                    ];

                    $this->upload->initialize($config);

                    $this->load->library('upload', $config);

                    $upload = $this->upload->do_upload();
                    print_r($this->upload->display_errors());

                endfor;
            }
            if (!$upload) {
                $this->session->set_flashdata("success_message", "Failed Upload");
                redirect('Reimburses/formReimburse', 'refresh');
            } else {
                $this->upload->data('file_name');
                $save = $this->reimburseModel->saveReimburseAll('reimburse', $data);
                if (!$save) {
                    $this->session->set_flashdata("error_message", "Failed To Save");
                    redirect('Reimburses/formReimburse', 'refresh');
                } else {
                    $this->session->set_flashdata("success_message", "Success To Upload Reimburse");
                    redirect('Reimburses/index', 'refresh');
                }
            }
        }
    }

    // Function Delete
    public function deleteReimburse($iD)
    {
        $where = [
            'iD' => $iD
        ];
        $this->reimburseModel->deleteReimburse($where);

        $this->session->set_flashdata("success_message", "Delete Success");

        redirect('Reimburses/index', 'refresh');
    }


    // function category reimburse
    public function categoryReimburse()
    {
        $view_data['reimburse'] = $this->reimburseModel->reimburseInfo();
        $this->template->rander('reimburse/formCategory', $view_data);
    }

    // function Save category
    public function saveCategory()
    {
        $this->form_validation->set_rules('category', 'category', 'required');
        if($this->form_validation->run() == False ){
            $this->session->set_flashdata('error_message', 'Please List Every Field');
            redirect('Rimburses/categoryReimburse');
        }else{
            $data = [
                'category' => $this->input->post('category')
            ];
            
            $save = $this->reimburseModel->saveReimburse('category_reimburse', $data);
            if (!$save) {
                $this->session->set_flashdata("error_message", "Failed To Save Category");
                redirect('Reimburses/index', 'refresh');
            } else {
                $this->session->set_flashdata("success_message", "Success To Add New Category");
                redirect('Reimburses/index', 'refresh');
            }
        }
    }


    // function edit reimburse 
    public function editReimburse($iD)
    {
        $model_info = $this->reimburseModel->get_one(($iD));

        $view_data['Reimburse'] = $this->reimburseModel->reimburseInfo();

        $model_info->category_reimburse_id = $model_info->category_reimburse_id;

        $view_data['model_info'] = $model_info;
        $view_data['members_dropdown'] = $this->Users_model->get_all_where(array("deleted" => 0, "user_type" => "staff"))->result();
        $view_data["category"] = $this->reimburseModel->categoryReimburse();

        if ($model_info->status_reimburse === "Approved" || $model_info->status_reimburse === "Declined") {
            $this->session->set_flashdata('error_message', 'This Reimburse Has Been Processed');
            redirect('Reimburses/index', 'refresh');
        } else {
            $this->template->rander('reimburse/editReimburse', $view_data);
        }
    }

    // function Submit Edit
    public function submitEdit()
    {

        $this->form_validation->set_rules('No_reimburse', 'No_reimburse', 'required');
        // $this->form_validation->set_rules('reimburse_roles_id', 'reimburse_roles_id', 'required');
        $this->form_validation->set_rules('nama_id', 'nama_id', 'required');
        $this->form_validation->set_rules('category_reimburse_id', 'category_reimburse_id', 'required');
        $this->form_validation->set_rules('amount', 'amount', 'required|numeric');
        $this->form_validation->set_rules('date_reimburse', 'date_reimburse', 'required');
        if ($this->form_validation->run() == False) {
            $this->session->set_flashdata('error_message', 'Please List Every Field');
            redirect('Reimburses/editReimburse');
        } else {
            $data = [
                'No_reimburse' => $this->input->post('No_reimburse'),
                'reimburse_roles_id' => $this->input->post('reimburse_roles_id'),
                'nama_id' => $this->input->post('nama_id'),
                'category_reimburse_id' => $this->input->post('category_reimburse_id'),
                'amount' => $this->input->post('amount'),
                'date_reimburse' => $this->input->post('date_reimburse'),
                'status_reimburse' => 'Pending',
                'photo' => $_FILES['photo']['name'],
                // 'nama' => $this->input->post('nama'),
            ];
            $where = [
                'iD' => $this->input->post('iD')
            ];


            // condition
            $model_info = $this->reimburseModel->get_one(($this->input->post('iD')));

            $date = $model_info->date_publish;
            $date = strtotime($date);
            $date = strtotime('-7 day', $date);

            if ($data['date_reimburse'] < date('Y-m-d', $date)) {
                $this->session->set_flashdata("error_message", "Reimburse Max 1 Week Ago");
                redirect('Reimburses/index', 'refresh');
            } else {
                // for($i = 1; $_FILES['photo'] > $i)
                $image_path = realpath(APPPATH . '../assets/reimburse/');

                $config = [
                    'upload_path' => $image_path,
                    'allowed_types' => 'jpg|png|gif',
                    'overwrite' => TRUE
                ];
                $this->upload->initialize($config);

                $this->load->library('upload', $config);

                $upload = $this->upload->do_upload('photo');
                if ($data['photo'] !== "") {
                    if (!$upload) {
                        $this->session->set_flashdata("error_message", "Failed To Upload");
                        redirect('Reimburses/index', 'refresh');
                    } else {
                        if ($model_info->status_reimburse === "Pending") {
                            $this->upload->data('file_name');
                            $save = $this->reimburseModel->editData($where, $data, 'reimburse');
                            $this->session->set_flashdata("success_message", "Success To Update Data");
                            redirect('Reimburses/index', 'refresh');
                        } else {
                            $this->session->set_flashdata("error_message", "This Reimburse Has Been Processed");
                            redirect('Reimburses/index', 'refresh');
                        }
                    }
                } else {
                    unset($data['photo']);
                    if ($model_info->status_reimburse === "Pending") {
                        $save = $this->reimburseModel->editData($where, $data, 'reimburse');
                        $this->session->set_flashdata("success_message", "Success To Update Data");
                        redirect('Reimburses/index', 'refresh');
                    } else {
                        $this->session->set_flashdata("error_message", "This Reimburse Has Been Processed");
                        redirect('Reimburses/index', 'refresh');
                    }
                }
            }
        }
    }

    public function seeDetail($iD)
    {
        $model_info = $this->reimburseModel->get_one(($iD));

        $view_data['Reimburse'] = $this->reimburseModel->reimburseInfo();

        $model_info->category_reimburse_id = $model_info->category_reimburse_id;

        $view_data['model_info'] = $model_info;
        $view_data['members_dropdown'] = $this->Users_model->get_all_where(array("deleted" => 0, "user_type" => "staff"))->result();
        $view_data["category"] = $this->reimburseModel->categoryReimburse();

        $this->load->view('reimburse/seeDetail', $view_data);
    }

    public function Approve($iD)
    {
        $view_data['reimburse'] = $this->reimburseModel->select_where_reimburse($iD)->row();
        if ($this->login_user->is_admin || $this->login_user->user_type === "finance") {

            $this->template->rander('reimburse/approveReimburse', $view_data);
        } else {
            $this->session->set_flashdata("error_message", "You're not allowed to access this page");
            redirect('Reimburses/index');
        }
    }

    public function processReimburse()
    {
        $model_info = $this->reimburseModel->get_one($this->input->post('iD'));
        if ($this->input->post('btnReimburse') === "Approve") {
            $where = [
                'iD' => $this->input->post('iD')
            ];
            $data = [
                'status_reimburse' => 'Approved'
            ];
            $this->reimburseModel->updateOneReimburse('reimburse', 'status_reimburse', $where, $data);

            $this->session->set_flashdata('success_message', 'Reimburse Has Been Approved');
            redirect('Reimburses/index', 'refresh');
        } elseif ($this->input->post('btnReimburse') === "Decline") {
            $where = [
                'iD' => $this->input->post('iD')
            ];
            $data = [
                'status_reimburse' => 'Declined'
            ];
            $this->reimburseModel->updateOneReimburse('reimburse', 'status_reimburse', $where, $data);

            $this->session->set_flashdata('error_message', 'Reimburse Has Been Rejected');
            redirect('Reimburses/index', 'refresh');
        }
    }
}
