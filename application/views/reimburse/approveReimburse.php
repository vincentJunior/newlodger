<?php echo form_open_multipart(get_uri("Reimburses/processReimburse"), array("id" => "formReimburse", "class" => "general-form", "role" => "form")); ?>
<div id="page-content" class="clearfix p20">
    <div class="panel clearfix">
        <ul id="expenses-tabs" class="nav nav-tabs bg-white title" role="tablist">
            <li class="title-tab">
                <h4 class="pl15 pt10 pr15">Reimburse Detail</h4>
            </li>
            <div class="tab-title clearfix no-border">
                <div class="title-button-group">
                    <?php echo anchor(get_uri("Reimburses/index"), "Back", array("class" => "btn btn-warning")); ?>
                </div>
            </div>
        </ul>
        <input type="hidden" name="iD" value="<?php echo $reimburse->iD ?>">
        <div class="container">
            <table class="table table-dark">
                <tr>
                    <td>No Surat</td>
                    <td><?php echo 'R - ' . $reimburse->iD . ' - ' . date("d - m - Y", strtotime($reimburse->date_publish)) ?></td>
                </tr>
                <tr>
                    <td>No Reimburse</td>
                    <td><?php echo $reimburse->No_reimburse ?></td>
                </tr>
                <tr>
                    <td>Nama</td>
                    <td><?php echo $reimburse->first_name . ' ' . $reimburse->last_name ?></td>
                </tr>
                <tr>
                    <td>Category</td>
                    <td><?php echo $reimburse->category ?></td>
                </tr>
                <tr>
                    <td>Amount</td>
                    <td><?php echo $reimburse->amount ?></td>
                </tr>
                <tr>
                    <td>Jabatan</td>
                    <td><?php echo $reimburse->title ?></td>
                </tr>
                <tr>
                    <td>Tanggal Reimburse</td>
                    <td><?php echo $reimburse->date_reimburse ?></td>
                </tr>
                <tr>
                    <td>Photo</td>
                    <td><img src="<?php echo base_url('assets/reimburse/') . $reimburse->photo ?>" style="width: 550px; height: 309,38px;"> </td>
                </tr>
            </table>
            <div class="button">'
                <button class="btn btn-danger" name="btnReimburse" value ="Decline">Decline</button>
                <button class="btn btn-primary" name="btnReimburse" value ="Approve">Approve</button>
            </div>
        </div>
    </div>
</div>

<?php form_close() ?>