<?php echo form_open_multipart(get_uri("Reimburses/saveCategory"), array("id" => "formReimburse", "class" => "general-form", "role" => "form")); ?>
<div id="expense-dropzone" class="post-dropzone">
    <div class="modal-body clearfix">
        <!-- <form action =" " method='POST'> -->
        <div class="form-group">
            <label for="category">Category</label>
            <input type="text" class="form-control" id="category" name="category" placeholder="Insert Category">
        </div>
        <br>
        <br>
        <br>
        <div class="float-right">
            <?php echo anchor(get_uri("Reimburses/index"), "" . "  Back  ", array("class" => "btn btn-warning", "title" => "Back")); ?>
            <button type="submit" class="btn btn-primary ">Submit</button>
        </div>
        <!-- </form> -->
    </div>
</div>
<?php echo form_close() ?>


<script type="text/javascript">
    $(document).ready(function() {


        var uploadUrl = "<?php echo get_uri("expenses/upload_file"); ?>";
        var validationUrl = "<?php echo get_uri("expenses/validate_expense_file"); ?>";

        var dropzone = attachDropzoneWithForm("#expense-dropzone", uploadUrl, validationUrl);

        $("#expense-form").appForm({
            onSuccess: function(result) {
                if (typeof $EXPENSE_TABLE !== 'undefined') {
                    $EXPENSE_TABLE.appTable({
                        newData: result.data,
                        dataId: result.id
                    });
                } else {
                    location.reload();
                }
            }
        });

        setDatePicker("#expense_date");

        $("#expense-form .select2").select2();

    });
</script>