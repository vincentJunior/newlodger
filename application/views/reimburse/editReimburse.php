<?php echo form_open_multipart(get_uri("Reimburses/submitEdit"), array("id" => "formReimburse", "class" => "general-form", "role" => "form")); ?>
<div id="expense-dropzone" class="post-dropzone">
    <div class="modal-body clearfix">
        <!-- <form action =" " method='POST'> -->
        <div class="form-group">
            <input type="hidden" name="iD" value="<?php echo $model_info->iD ?>">
        </div>
        <div class="from-group">
            <label for="No">No </label>
            <input type="text" class="form-control" name="No_reimburse" placeholder="No Reimburse" value="<?php echo $model_info->No_reimburse ?>">
        </div>
        <br>
        <div class="form-group">
            <label for="category_reimburse_id">Jabatan</label>
            <select class="form-control form-control-lg" name="reimburse_roles_id">
                <option value="">-</option>
                <?php
                foreach ($jabatan as $role) { ?>
                    <option value="<?php echo $role->id ?>"><?php echo $role->title ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="nama_id">User</label>
            <select class="form-control form-control-lg" name="nama_id">
                <option value="">-</option>
                <?php
                foreach ($members_dropdown as $md) { ?>
                    <option value="<?php echo $md->id ?>"><?php echo $md->first_name . " " . $md->last_name ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="category_reimburse_id">Category</label>
            <select class="form-control form-control-lg" name="category_reimburse_id">
                <option value="">-</option>

                <?php
                foreach ($category as $ct) { ?>
                    <option value="<?php echo $ct->id ?>">
                        <?php echo $ct->category ?>
                    </option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="amount">Amount</label>
            <input type="text" class="form-control" id="amount" name="amount" placeholder="Amount" value="<?php echo $model_info->amount ?>">
        </div>
        <div class="form-group">

            <label for="date_reimburse">Date</label>
            <input type="date" class="form-control" id="date_reimburse" name="date_reimburse" value='<?php echo date("Y-m-d", strtotime($model_info->date_publish)) ?>'>

        </div>
        <div class="form-group">
            <div class="form-group">
                <label for="photo">Input Photo</label>
                <input type="file" class="form-control-file" id="photo" name="photo" size='1'>
            </div>
        </div>
        <div class="float-right">
            <?php echo anchor(get_uri("Reimburses/index"), "" . "  Back  ", array("class" => "btn btn-warning", "title" => "Back")); ?>
            <button type="submit" class="btn btn-primary ">Submit</button>
        </div>
        <!-- </form> -->
    </div>
</div>
<?php echo form_close() ?>


<script type="text/javascript">
    $(document).ready(function() {


        var uploadUrl = "<?php echo get_uri("expenses/upload_file"); ?>";
        var validationUrl = "<?php echo get_uri("expenses/validate_expense_file"); ?>";

        var dropzone = attachDropzoneWithForm("#expense-dropzone", uploadUrl, validationUrl);

        $("#expense-form").appForm({
            onSuccess: function(result) {
                if (typeof $EXPENSE_TABLE !== 'undefined') {
                    $EXPENSE_TABLE.appTable({
                        newData: result.data,
                        dataId: result.id
                    });
                } else {
                    location.reload();
                }
            }
        });

        setDatePicker("#expense_date");

        $("#expense-form .select2").select2();

    });
</script>