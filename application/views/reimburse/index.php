<div id="page-content" class="clearfix p20">
    <div class="panel clearfix">
        <ul id="expenses-tabs" class="nav nav-tabs bg-white title" role="tablist">
            <li class="title-tab">
                <h4 class="pl15 pt10 pr15"><?php echo lang("expenses"); ?></h4>
            </li>
            <div class="tab-title clearfix no-border">
                <div class="title-button-group">
                    <?php echo anchor(get_uri("Reimburses/categoryReimburse"), "<i class='fa fa-plus-circle'></i> " . "Add Category", array("class" => "btn btn-default mb0", "title" => "Add Category")); ?>
                    <?php echo anchor(get_uri("Reimburses/formReimburse"), "<i class='fa fa-plus-circle'></i> " . "Add Reimburse", array("class" => "btn btn-default mb0", "title" => "Add Reimburse")); ?>
                </div>
            </div>
        </ul>
        <table class="table table-dark">
            <thead>
                <tr>
                    <th scope="col">No</th>
                    <th scope="col">Name</th>
                    <th scope="col">Nomor Surat</th>
                    <th scope="col">Category</th>
                    <th scope="col">Date Reimburse</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Status</th>
                    <th scope="col">Photo</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                $no = 1;
                foreach($reimburse as $R){ ?>
                <tr>
                    <th scope="row"><?php echo $no++ ?></th>
                    <td><?php echo $R->first_name . ' ' . $R->last_name?></td>
                    <td><?php echo 'R - '.$R->iD . '- '. date("d-m-Y", strtotime($R->date_publish)) ?></td>
                    <td><?php echo $R->category ?></td>
                    <td><?php echo $R->date_reimburse ?></td>
                    <td><?php echo 'Rp.'.$R->amount ?></td>
                    <td><a><?php echo $R->status_reimburse ?></a></td>
                    <td><img src ="<?php echo base_url('assets/reimburse/').$R->photo ?>" width ="45" height ="45" ></td>
                    <td>
                        <?php echo anchor(get_uri("Reimburses/editReimburse/".$R->iD ), "<i class=''></i> " . "Edit", array("class" => "btn btn-warning ml 2", "title" => "Edit")); ?>
                        <?php echo anchor(get_uri("Reimburses/deleteReimburse/".$R->iD ), "<i class=''></i> " . "Delete", array("class" => "btn btn-danger", "title" => "Delete")); ?>
                    </td>

                </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>



<script type="text/javascript">
    loadExpensesTable = function(selector, dateRange) {
        var customDatePicker = "";
        if (dateRange === "custom") {
            customDatePicker = [{
                startDate: {
                    name: "start_date",
                    value: moment().format("YYYY-MM-DD")
                },
                endDate: {
                    name: "end_date",
                    value: moment().format("YYYY-MM-DD")
                },
                showClearButton: true
            }];
            dateRange = "";
        }

        $(selector).appTable({
            source: '<?php echo_uri("expenses/list_data") ?>',
            dateRangeType: dateRange,
            filterDropdown: [{
                    name: "category_id",
                    class: "w200",
                    options: <?php echo $categories_dropdown; ?>
                },
                {
                    name: "user_id",
                    class: "w200",
                    options: <?php echo $members_dropdown; ?>
                },
                <?php if ($projects_dropdown) { ?> {
                        name: "project_id",
                        class: "w200",
                        options: <?php echo $projects_dropdown; ?>
                    }
                <?php } ?>
            ],
            order: [
                [0, "asc"]
            ],
            rangeDatepicker: customDatePicker,
            columns: [{
                    visible: false,
                    searchable: false
                },
                {
                    title: '<?php echo lang("date") ?>',
                    "iDataSort": 0
                },
                {
                    title: '<?php echo lang("category") ?>'
                },
                {
                    title: '<?php echo lang("title") ?>'
                },
                {
                    title: '<?php echo lang("description") ?>'
                },
                {
                    title: '<?php echo lang("files") ?>'
                },
                {
                    title: '<?php echo lang("amount") ?>',
                    "class": "text-right"
                },
                {
                    title: '<?php echo lang("tax") ?>',
                    "class": "text-right"
                },
                {
                    title: '<?php echo lang("second_tax") ?>',
                    "class": "text-right"
                },
                {
                    title: '<?php echo lang("total") ?>',
                    "class": "text-right"
                }
                <?php echo $custom_field_headers; ?>,
                {
                    title: '<i class="fa fa-bars"></i>',
                    "class": "text-center option w100"
                }
            ],
            printColumns: [1, 2, 3, 4, 6, 7, 8, 9],
            xlsColumns: [1, 2, 3, 4, 6, 7, 8, 9],
            summation: [{
                column: 6,
                dataType: 'currency'
            }, {
                column: 7,
                dataType: 'currency'
            }, {
                column: 8,
                dataType: 'currency'
            }, {
                column: 9,
                dataType: 'currency'
            }]
        });
    };

    $(document).ready(function() {
        loadExpensesTable("#monthly-expense-table", "monthly");
    });
</script>