<div class="container">
    <div class="row">
        <?php echo form_open_multipart(get_uri("Reimburses/saveReimburse"), array("id" => "formReimburse", "class" => "form-reimburse", "role" => "form")); ?>
        <?php echo validation_errors() ?>)
        <div class="col-xs-12">
            <div class="form-block">
                <div class="from-group col-md-3">
                    <label for="No">No </label>
                    <input type="text" class="form-control" name="No_reimburse">
                </div>
                <div class="form-group col-md-3">
                    <label for="category_reimburse_id">Jabatan</label>
                    <select class="form-control form-control-lg" name="reimburse_role_id">
                        <option value="">-</option>
                        <?php
                        foreach ($jabatan as $role) { ?>
                            <option value="<?php echo $role->id ?>"><?php echo $role->title ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="category_reimburse_id">User</label>
                    <select class="form-control form-control-lg" name="nama_id">
                        <option value="">-</option>
                        <?php
                        foreach ($members_dropdown as $md) { ?>
                            <option value="<?php echo $md->id ?>"><?php echo $md->first_name . " " . $md->last_name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="form-group col-md-3">
                    <label for="amount">Total Amount</label>
                    <output id="total" class="form-control" width="100" height="50"></output>
                </div>
                <div class="form-main">
                    <div class="form-group col-md-2">
                        <label for="date_reimburse">Date</label>
                        <input type="date" class="form-control Date_reimburse" id="datepicker" name="date_reimburse[]" value='<?php echo date('Y-m-d') ?>'>
                    </div>
                    <div class="form-group col-md-2">
                        <div class="custom-file">
                            <label class="custom-file-label" for="inputGroupFile03">Choose file</label>
                            <input type="file" class="custom-file-input" id="photo" name="photo[]">
                        </div>
                    </div>
                    <div class="form-group col-md-2">
                        <label for="category_reimburse_id">Category</label>
                        <select class="form-control form-control-lg" id="category" name="category_reimburse_id[]">
                            <option value="">-</option>
                            <?php
                            foreach ($category as $ct) { ?>
                                <option value="<?php echo $ct->id ?>"><?php echo $ct->category ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="keterangan">Keterangan</label>
                        <input type="text" class="form-control" id="keterangan" name="keterangan[]">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="amount">Amount</label>
                        <input type="text" class="form-control amount" id="amount" name="amount[]" placeholder="Amount">
                    </div>
                </div>
                <hr>
            </div>
            <div class="button">
                <div class="container">
                    <a class="btn btn-primary add-more-btn">Add More </a>
                    <a class="btn btn-warning btn-reset-form">Delete Form </a>
                </div>
            </div>
            <div class="button ">
                <div class="container ">
                    <br>
                    <button class="btn btn-primary"> Submit </button>
                    <?php echo anchor(get_uri("Reimburses/index"), "" . "  Back  ", array("class" => "btn btn-warning", "title" => "Back")); ?>
                </div>
                <br>
            </div>
        </div>
    </div>
</div>
</div>
<br>
<? form_close() ?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {

        // $('body').on('focus', ".Date_reimburse", function() {
        //     $(this).datepicker({
        //         minDate: -7,
        //         maxDate: "+1M +10D"
        //     });
        // });

        // $('.add-more-btn').click(function() {

        //     // Create clone of <div class='input-form'>
        //     var formBlock = $('.form-block');
        //     var formMain = $('.form-main').clone();
        //     formBlock.append(formMain.html());
        // });

        $(".add-more-btn, .check ").click(function() {
            $('.form-main:last')
                .clone(true)
                .insertAfter('.form-main:last').find('input').each(function() {
                    $(this).val('');
                });
        });


        $(".btn-reset-form").click(function() {
            $('.form-main:last').remove();
        });
        $(".form-group").on('input', '.amount', function() {
            var totalSum = 0;
            $('.amount').each(function() {
                var inputVal = $(this).val();
                totalSum += parseFloat(inputVal);
            });
            $('#total').text(totalSum);
        })



    });
</script>