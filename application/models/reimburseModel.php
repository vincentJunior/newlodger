<?php

class reimburseModel extends Crud_model
{
    private $table;

    function __construct()
    {
        $this->table = 'reimburse';
        parent::__construct($this->table);
    }


    function saveReimburse($table, $data)
    {
        $this->load->database('default', TRUE);
        if (!$this->db->insert($table, $data))
            return FALSE;
        $data["id"] = $this->db->insert_id();

        return (object) $data;
    }

    function getJabatan()
    {
        $this->load->database('default', TRUE);
        $this->db->select('*');
        $this->db->from('roles');
        $data = $this->db->get();

        return $data->result();
    }


    function saveReimburseAll($table, $data)
    {

        return $this->db->insert_batch($table, $data);
    }

    function categoryReimburse()
    {
        $this->load->database('default', TRUE);
        $this->db->select('*');
        $this->db->from('category_reimburse');
        $data = $this->db->get();
        return $data->result();
    }


    function reimburseInfo()
    {
        $this->load->database('default', TRUE);
        $this->db->select('*');
        $this->db->from('reimburse');
        $this->db->join('category_reimburse', 'reimburse.category_reimburse_id = category_reimburse.id', 'LEFT');
        $this->db->join('users', 'reimburse.nama_id = users.id', 'LEFT');
        $data = $this->db->get();
        return $data->result();
    }

    function deleteReimburse($where)
    {
        $this->db->where($where);
        $this->db->delete('reimburse');
    }

    function getDataEdit($table, $where)
    {
        return $this->db->get_where($table, $where);
    }

    function select_where_reimburse($iD)
    {
        $this->load->database('default', TRUE);
        $this->db->select('*');
        $this->db->from('reimburse');
        $this->db->join('category_reimburse', 'reimburse.category_reimburse_id = category_reimburse.id', 'LEFT');
        $this->db->join('users', 'reimburse.nama_id = users.id', 'LEFT');
        $this->db->join('roles', 'reimburse.reimburse_roles_id = roles.id', 'LEFT');
        $this->db->where(array('reimburse.id' => $iD));
        // $this->db->join('roles', 'reimburse.nama_id = sers.id', 'LEFT');
        $data = $this->db->get();
        if (!$data) {
            return $this->db->error();
            exit;
        } else {
            return $data;
        }
    }

    function editData($where, $data, $table)
    {
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table);
    }

    function select_where($table, $column, $where)
    {
        $this->load->database('default', TRUE);
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($column, $where);
        $data = $this->db->get();
        return $data;
    }

    function updateOneReimburse($table, $column, $where,$data)
    {
        $this->db->set($data);
        $this->db->where($where);
        return $this->db->update($table);

    }
}
